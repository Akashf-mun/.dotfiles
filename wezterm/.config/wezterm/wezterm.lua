local wezterm = require("wezterm")

local function scheme_for_appearance(appearance)
  if appearance:find "Dark" then
    return "GruvboxDark"
  else
    -- return "Gruvbox Light"
    return "GruvboxDark"
  end
end

return {
    enable_wayland = false, -- Fixes hyprland issues *Somewhat still laggy
    -- front_end = "WebGpu",
    font = wezterm.font("FiraCode Nerd Font"),
    default_prog = { "/usr/bin/fish", "-l" },
    -- window_background_image = "/Users/akash/Documents/wallpapers/forest-sunset.jpg",
    color_scheme = scheme_for_appearance(wezterm.gui.get_appearance()),
    -- text_background_opacity = 0.6,
    native_macos_fullscreen_mode = true,
    -- window_background_opacity = 0.7,
    window_decorations ="RESIZE",
    enable_tab_bar = false,
    adjust_window_size_when_changing_font_size = false,
    unix_domains = {
        {name="unix"}
    },
}
