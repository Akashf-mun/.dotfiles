-- quick fix list keybindings
vim.keymap.set("n", "<leader>k", "<CMD>lnext<CR>")
vim.keymap.set("n", "<leader>j", "<CMD>lprev<CR>")
vim.keymap.set("n", "<C-k>", "<CMD>cnext<CR>")
vim.keymap.set("n", "<C-j>", "<CMD>cprev<CR>")

-- git keybindings
vim.keymap.set("n", "<leader>bl", "<cmd>Gitsigns blame_line<CR>")

-- lsp keybindings
-- Done localy in lsp file as they should only activate on attatch

-- telescope keybindings
vim.keymap.set("n", "<leader>fs", function() require("telescope/builtin").grep_string { search = vim.fn.expand("<cword>") } end)
vim.keymap.set("n", "<leader>fg", require("telescope/builtin").live_grep)
vim.keymap.set("n", "<leader>ff", require("telescope/builtin").find_files)
vim.keymap.set("n", "<leader>bb", require("telescope/builtin").buffers)
vim.keymap.set("n", "<leader>tl", require("telescope/builtin").builtin)
vim.keymap.set("n", "<leader>vrc", require("mr_fish/plugs/telescope").search_dotfiles)
