return {
    {
        "neovim/nvim-lspconfig",
        opts = {
            servers = {
                cmake = {},
                texlab = {},
                pyright = {},
                typst_lsp = {},
                lua_ls = {
                    settings = {
                        Lua = {
                            runtime = {
                                -- Tell the language server which version of Lua you're using (most likely LuaJIT in the case of Neovim)
                                version = "LuaJIT",
                            },
                            diagnostics = {
                                -- Get the language server to recognize the `vim` global
                                globals = { "vim" },
                            },
                            workspace = {
                                -- Make the server aware of Neovim runtime files
                                library = vim.api.nvim_get_runtime_file("", true),
                            },
                            -- Do not send telemetry data containing a randomized but unique identifier
                            telemetry = {
                                enable = false,
                            },
                        },
                    },
                }
            },
            setup = {},
        },
        config = function(_, opts)
            local key_opts = { noremap = true, silent = true }

            vim.keymap.set("n", "<leader>e", vim.diagnostic.open_float, key_opts)
            vim.keymap.set("n", "[d", vim.diagnostic.goto_prev, key_opts)
            vim.keymap.set("n", "]d", vim.diagnostic.goto_next, key_opts)
            vim.keymap.set("n", "<leader>q", require("telescope.builtin").diagnostics, key_opts)

            vim.api.nvim_create_autocmd("LspAttach", {
                callback = function(args)
                    local bufnr = args.buf
                    -- local client = vim.lsp.get_client_by_id(args.data.client_id)
                    vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc')

                    local bufopts = { noremap = true, silent = true, buffer = bufnr }
                    vim.keymap.set("n", "gS", vim.lsp.buf.workspace_symbol, bufopts)
                    vim.keymap.set("n", "gd", require("telescope.builtin").lsp_definitions, bufopts)
                    vim.keymap.set("n", "gD", vim.lsp.buf.declaration, bufopts)
                    vim.keymap.set("n", "gt", require("telescope.builtin").lsp_type_definitions, bufopts)
                    vim.keymap.set("n", "gi", vim.lsp.buf.implementation, bufopts)
                    vim.keymap.set("n", "gr", require("telescope.builtin").lsp_references, bufopts)
                    vim.keymap.set("n", "K", vim.lsp.buf.hover, bufopts)
                    vim.keymap.set("n", "<leader>D", vim.lsp.buf.type_definition, bufopts)
                    vim.keymap.set("n", "<leader>ca", vim.lsp.buf.code_action, bufopts)
                    vim.keymap.set("n", "<C-k>", vim.lsp.buf.signature_help, bufopts)
                    vim.keymap.set("n", "<leader>f", vim.lsp.buf.format, bufopts)
                    vim.keymap.set("n", "<leader>rn", vim.lsp.buf.rename, bufopts)
                end
            })

            local servers = opts.servers
            local capabilities = require("cmp_nvim_lsp").default_capabilities()

            local function setup(server)
                local server_opts = vim.tbl_deep_extend(
                    "force",
                    {
                        capabilities = vim.deepcopy(capabilities),
                    },
                    servers[server] or {}
                )
                if opts.setup[server] then
                    if opts.setup[server](server, server_opts) then
                        return
                    end
                elseif opts.setup["*"] then
                    if opts.setup["*"](server, server_opts) then
                        return
                    end
                end
                require("lspconfig")[server].setup(server_opts)
            end

            for server, server_opts in pairs(servers) do
                if server_opts then
                    server_opts = server_opts == true and {} or server_opts
                end
                setup(server)
            end
        end
    },
    "onsails/lspkind-nvim",
}
