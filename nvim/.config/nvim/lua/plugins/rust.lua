return {
    {
        "simrat39/rust-tools.nvim",
        dependencies = {
            "neovim/nvim-lspconfig",
            "nvim-lua/plenary.nvim",
            "mfussenegger/nvim-dap",
        },
        config = function()
            local rt = require("rust-tools")
            rt.setup({
                on_attach = function(_, bufnr)
                    local bufopts = { buffer = bufnr }
                    vim.keymap.set("n", "<leader>ca", rt.code_action_group.code_action_group, bufopts)
                    vim.keymap.set("n", "<leader>li", rt.inlay_hints.enable, bufopts)
                    vim.keymap.set("n", "<leader>lr", rt.runnables.runnables, bufopts)
                    vim.keymap.set("n", "<leader>le", rt.expand_macro.expand_macro, bufopts)
                    vim.keymap.set("n", "<leader>lo", rt.open_cargo_toml.open_cargo_toml, bufopts)
                    vim.keymap.set("n", "K", rt.hover_actions.hover_actions, bufopts)
                end
            })
        end,
    },
    {
        "saecki/crates.nvim",
        dependencies = {
            "nvim-lua/plenary.nvim",
        },
        version = "v0.3.0",
        config = function()
            require("crates").setup()
        end,
    },
}
