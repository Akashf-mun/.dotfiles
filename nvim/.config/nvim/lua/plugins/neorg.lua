return {
    {
        "nvim-neorg/neorg",
        dependencies = {
            "nvim-lua/plenary.nvim",
        },
        build = ":Neorg sync-parsers",
        opts = {
            load = {
                ["core.defaults"] = {}, -- Load all the default modules
                ["core.completion"] = {
                    config = {
                        engine = "nvim-cmp"
                    }
                },
                ["core.concealer"] = {
                    config = {

                    },
                },
                ["core.dirman"] = {
                    config = {
                        workspaces = {
                            my_workspace = "~/neorg",
                        },
                    },
                },
                ["core.presenter"] = {
                    config = {
                        zen_mode = "zen-mode",
                    },
                },
            },
        },
    },
}
