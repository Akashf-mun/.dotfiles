return {
    {
        "nvim-treesitter/nvim-treesitter",
        build = ":TSUpdate",
        config = function()
            require("nvim-treesitter.configs").setup({
                highlight = { enable = true },
                ensure_installed = {
                    "cpp",
                    -- "c",
                    "rust",
                    "comment",
                    "latex",
                    "bibtex",
                    -- "lua",
                    "bash",
                    "fish",
                    "toml",
                    "norg",
                    "wgsl",
                },
                auto_install = true,
                incremental_selection = {
                    enable = true,
                    keymaps = {
                        init_selection = "gnn",
                        node_incremental = "grn",
                        scope_incremental = "grc",
                        node_decremental = "grm",
                    }
                },
                indent = {
                    enable = true
                },
                rainbow = {
                    enable = true,
                    extended_mode = true,
                    max_file_lines = nil,
                },
            })
        end,
    }
}
