-- Maybe try and use smart column
return {
    {
        "folke/zen-mode.nvim",
        config = function()
            local zen_mode = require("zen-mode")

            zen_mode.setup {
                plugins = {
                    options = {
                        enabled = true,
                    },
                    tmux = { enable = true },
                },
            }

            vim.keymap.set(
                "n",
                "<leader>mz",
                zen_mode.toggle
            )
        end,
    },
    {
        "folke/twilight.nvim",
        config = function()
            local twilight = require("twilight")


            twilight.setup({
                -- context = 0
            })

            vim.keymap.set(
                "n",
                "<leader>mt",
                twilight.toggle
            )
        end,
    },
    "lukas-reineke/indent-blankline.nvim",
    "stevearc/dressing.nvim",
}
