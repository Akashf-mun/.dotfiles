return {
    {
        "nvim-telescope/telescope.nvim",
        tag = "0.1.1",
        dependencies = {
            "nvim-lua/plenary.nvim",
            "nvim-telescope/telescope-fzy-native.nvim",
            "nvim-tree/nvim-web-devicons",
        },
        config = function()
            local actions = require("telescope.actions")
            require("telescope").setup {
                defaults = {
                    --vimgrep_arguments = {
                    --  "rg",
                    --  "--color=never",
                    --  "--no-heading",
                    --  "--with-filename",
                    --  "--line-number",
                    --  "--column",
                    --  "--smart-case"
                    --},
                    prompt_prefix = "   ",
                    selection_caret = "  ",
                    entry_prefix = "  ",
                    initial_mode = "insert",
                    selection_strategy = "reset",
                    sorting_strategy = "ascending",
                    layout_strategy = "horizontal",
                    layout_config = {
                        horizontal = {
                            prompt_position = "top",
                            preview_width = 0.55,
                            results_width = 0.8,
                        },
                        vertical = {
                            mirror = false,
                        },
                        width = 0.87,
                        height = 0.80,
                        preview_cutoff = 120,
                    },
                    --file_sorter =  require"telescope.sorters".get_fuzzy_file,
                    file_sorter = require "telescope.sorters".get_fzy_sorter,
                    --file_ignore_patterns = {},
                    --generic_sorter =  require"telescope.sorters".get_generic_fuzzy_sorter,
                    --winblend = 0,
                    --border = {},
                    -- borderchars = { "─", "│", "─", "│", "╭", "╮", "╯", "╰" },
                    color_devicons = true,
                    --use_less = true,
                    --path_display = {},
                    --set_env = { ["COLORTERM"] = "truecolor" }, -- default = nil,
                    file_previewer = require "telescope.previewers".vim_buffer_cat.new,
                    grep_previewer = require "telescope.previewers".vim_buffer_vimgrep.new,
                    qflist_previewer = require "telescope.previewers".vim_buffer_qflist.new,
                    mappings = {
                        i = {
                            ["<C-x>"] = false,
                            ["<C-q>"] = actions.send_to_qflist,
                        },
                    },
                },
                pickers = {
                    lsp_code_actions = {
                        theme = "cursor",
                    },
                    diagnostics = {
                        theme = "dropdown",
                    },
                    live_grep = {
                        theme = "dropdown",
                    },
                    grep_string = {
                        theme = "dropdown",
                    },
                },
                extensions = {
                    fzy_native = {
                        override_generic_sorter = false,
                        override_file_sorter = true,
                    },
                },
            }

            require("telescope").load_extension("fzy_native")
            require("telescope").load_extension("harpoon")
            require("telescope").load_extension("dap")
        end,
    },
}
