return {
    {
        "lewis6991/gitsigns.nvim",
        config = function()
            require("gitsigns").setup()
        end,
    },
    {
        "TimUntersberger/neogit",
        dependencies = {
            "nvim-lua/plenary.nvim",
        },
        config = function()
            local neogit = require("neogit")

            neogit.setup({
                integrations = {
                    diffview = true,
                },
            })

            vim.keymap.set("n", "<leader>gg", neogit.open)
        end,
    },
    {
        "sindrets/diffview.nvim",
        dependencies = {
            "nvim-lua/plenary.nvim",
        },
    },
}
