return {
    {
        "ellisonleao/gruvbox.nvim",
        lazy = false,
        priority = 1000,
        config = function()
            vim.o.background = "dark"
            vim.cmd.colorscheme("gruvbox")
        end,
    },
    "folke/tokyonight.nvim",
    "savq/melange-nvim",
    "shaunsingh/nord.nvim",
    "sainnhe/gruvbox-material",
    {
        "rose-pine/neovim",
        name = "rose-pine",
    },
    {
        "catppuccin/nvim",
        name = "catppuccin",
    },
    "rebelot/kanagawa.nvim",
}
