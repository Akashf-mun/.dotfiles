return {
    {
        "L3MON4D3/LuaSnip",
        version = "1.*",
        dependencies = {
            "saadparwaiz1/cmp_luasnip",
        },
        config = function()
            local ls = require("luasnip")
            local s = ls.snippet
            local sn = ls.snippet_node
            local isn = ls.indent_snippet_node
            local t = ls.text_node
            local i = ls.insert_node
            local f = ls.function_node
            local c = ls.choice_node
            local d = ls.dynamic_node
            local r = ls.restore_node
            local events = require("luasnip.util.events")
            local types = require("luasnip.util.types")
            local ai = require("luasnip.nodes.absolute_indexer")

            ls.config.set_config({
                history = true,
                updateevents = "TextChanged,TextChangedI",
                enable_autosnippets = true,
                ext_opts = {
                    [types.choiceNode] = {
                        active = {
                            virt_text = { { "<-", "Error" } },
                        }
                    }
                }
            })

            vim.keymap.set({ "i", "s" }, "<C-k>", function()
                if ls.expand_or_jumpable() then
                    ls.expand_or_jump()
                end
            end, { silent = true })

            vim.keymap.set({ "i", "s" }, "<C-j>", function()
                if ls.jumpable(-1) then
                    ls.jump(-1)
                end
            end, { silent = true })

            vim.keymap.set({ "i", "s" }, "<C-l>", function()
                if ls.choice_active() then
                    ls.change_choice(1)
                end
            end, { silent = true })

            -- #ifdef __cplusplus
            -- extern "C" {
            -- #endif

            -- #ifdef __cplusplus
            -- }
            -- #endif

            local function cdocsnip(args, _)
                local nodes = {
                    t({ "/**", " * " }),
                    r(1, "discription", i(nil, "A short Description")),
                    t({ "", "" }),
                }

                local node_index = 2

                if string.find(args[2][1], ", ") then
                    vim.list_extend(nodes, { t({ " *", "" }) })
                end

                for index, arg in ipairs(vim.split(args[2][1], ", ", true)) do
                    local param_info = vim.split(arg, " ", true)

                    for index, arg in ipairs(param_info) do
                        if arg == "const" then
                            table.remove(param_info, index)
                        end
                    end

                    local param_name = param_info[2]

                    if param_name then
                        local inode = r(node_index, param_name, i(nil))
                        vim.list_extend(
                            nodes,
                            { t({ " * @param " .. param_name .. " " }), inode, t({ "", "" }) }
                        )
                        node_index = node_index + 1
                    end
                end

                if args[1][1] ~= "void" then
                    local inode = r(node_index, "return", i(nil))
                    vim.list_extend(
                        nodes,
                        { t({ " *", " * @return " }), inode, t({ "", "" }) }
                    )
                end

                vim.list_extend(nodes, { t({ " */", "" }) })

                -- local param_nodes = {}

                local snip = sn(nil, nodes)
                return snip
            end

            ls.add_snippets("c", {
                -- The guard needed around C code when used from C++
                s("c_guard", {
                    t({ "#ifdef __cplusplus", "" }),
                    c(1, {
                        t("extern \"C\" {"),
                        t("}"),
                    }),
                    t({ "", "#endif" }),
                }),

                s("fn", {
                    d(4, cdocsnip, { 1, 3 }),
                    c(1, {
                        t("void"),
                        i(nil, ""),
                    }),
                    -- i(1, "void"),
                    t(" "),
                    i(2, "myFunc"),
                    t("("),
                    i(3),
                    t(");"),
                }),

                s("paren_change", {
                    c(1, {
                        sn(nil, { t("("), r(1, "user_text"), t(")") }),
                        sn(nil, { t("["), r(1, "user_text"), t("]") }),
                        sn(nil, { t("{"), r(1, "user_text"), t("}") }),
                    }),
                }, {
                    stored = {
                        user_text = i(1, "default_text")
                    }
                })
            })

            ls.filetype_extend("cpp", { "c" })
            ls.filetype_extend("cu", { "c", "cpp" })
        end,
    }
}
