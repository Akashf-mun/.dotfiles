return {
    {
        "hrsh7th/nvim-cmp",
        dependencies = {
            "hrsh7th/cmp-nvim-lsp",
            "hrsh7th/cmp-buffer",
            "hrsh7th/cmp-path",
            "hrsh7th/cmp-cmdline",
            "hrsh7th/cmp-nvim-lsp-signature-help",
            -- depend on luasnip as well,
            -- {
            --     "L3MON4D3/LuaSnip",
            --     dependencies = {
            --         "saadparwaiz1/cmp_luasnip",
            --     },
            -- },
        },
        config = function()
            local luasnip = require("luasnip")
            local lspkind = require("lspkind")
            lspkind.init()

            local cmp = require("cmp")
            cmp.setup({
                snippet = {
                    expand = function(args)
                        luasnip.lsp_expand(args.body)
                    end,
                },
                mapping = {
                    ["<C-p>"] = cmp.mapping.select_prev_item(),
                    ["<C-n>"] = cmp.mapping.select_next_item(),
                    -- cmp.mapping tesc
                    ["<C-d>"] = cmp.mapping.scroll_docs(-4),
                    ["<C-u>"] = cmp.mapping.scroll_docs(4),
                    ["<C-Space>"] = cmp.mapping.complete({}),
                    ["<C-e>"] = cmp.mapping({
                        i = cmp.mapping.abort(),
                        c = cmp.mapping.close(),
                    }),
                    ["<CR>"] = cmp.mapping.confirm({
                        behavior = cmp.ConfirmBehavior.Replace,
                        select = false,
                    }),
                },
                sources = cmp.config.sources({
                    { name = "nvim_lsp" },
                    { name = "nvim_lsp_signature_help" },
                    { name = "nvim_lua" },
                    { name = "luasnip" },
                    { name = "neorg" },
                    { name = "crates" },
                    { name = "path" },
                }, {
                    { name = "buffer", keyword_length = 5 },
                }),
                formatting = {
                    format = lspkind.cmp_format({
                        with_text = true,
                        menu = ({
                            buffer = "[buf]",
                            nvim_lsp = "[LSP]",
                            luasnip = "[LuaSnip]",
                        })
                    }),
                }
            })
        end,
    }
}
