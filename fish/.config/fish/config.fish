fish_add_path $HOME/.cargo/bin
fish_add_path $HOME/.local/bin
fish_add_path /opt/cuda/bin
setenv EDITOR nvim
setenv STARSHIP_CONFIG $HOME/.config/starship/config.toml
setenv DOTFILES $HOME/.dotfiles
setenv XDG_CONFIG_HOME $HOME/.config
setenv JDTLS_HOME /usr/share/java/jdtls
setenv FZF_DEFAULT_OPTS "--layout=reverse --border=rounded"
setenv MANPAGER "sh -c 'col -bx | bat -l man -p'"
fish_vi_key_bindings

set -U fish_greeting

bind -M insert \cf "tmux-sessionizer"

abbr -a vi nvim
abbr -a vim nvim
abbr -a ls "exa -F"
abbr -a la "exa -alF --icons --header --git"
abbr -a tree "exa --tree"

alias config "cd ~/.config"

starship init fish | source
